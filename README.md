# Katabastird: a simple countdown timer

Katabastird is a simple countdown timer that is configured and launched from the commandline.

``` text
Usage: katabastird [OPTIONS]

Options:
  -h, --hours <HOURS>      Hours to count down
  -m, --minutes <MINUTES>  Minutes to count down
  -s, --seconds <SECONDS>  Seconds to count down
  -a, --alarm <ALARM>      Audio file to play at the end of the countdown
  -A
  -r, --running            Begin countdown immediately
  -u, --count-up           Count up from zero, actually
  -p, --predator           Use the Predator font
  -H, --help               Print this help
  -V, --version            Print version information
```

![a Predator's view of the timer while it's paused](./predator_timer_small.png "a Predator's view of rthe timer while it's paused")

"[Katabasis](https://en.wikipedia.org/wiki/Katabasis)" is the descent into the Underworld.

All content is licensed under the Chaos License; see [LICENSE.md](./LICENSE.md) for details.
