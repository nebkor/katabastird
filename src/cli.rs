use std::ffi::OsString;

use clap::Parser;

#[derive(Debug, Parser)]
#[clap(author, version, about, disable_help_flag = true)]
pub struct Cli {
    /// Hours to count down.
    #[clap(long, short)]
    pub hours: Option<u64>,

    /// Minutes to count down.
    #[clap(long, short)]
    pub minutes: Option<u64>,

    /// Seconds to count down.
    #[clap(long, short)]
    pub seconds: Option<u64>,

    /// Audio file to play at the end of the countdown.
    #[clap(long, short)]
    pub alarm: Option<OsString>,

    #[clap(short = 'A', conflicts_with = "alarm")]
    pub airhorn: bool,

    /// Begin countdown immediately.
    #[clap(long = "immediate", long = "running", short = 'i', short = 'r')]
    pub running: bool,

    /// Count up from zero, actually.
    #[clap(long, short = 'u')]
    pub count_up: bool,

    /// Use the Predator font.
    #[clap(long, short)]
    pub predator: bool,

    /// Print this help.
    #[clap(long, short = 'H', action = clap::ArgAction::Help)]
    pub help: Option<bool>,
}
