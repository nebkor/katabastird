use std::{io::Cursor, time::Duration};

use eframe::egui::{Color32, Direction, FontId, Layout, RichText};
use egui_extras::{Size, Strip};
use rodio::{source::Source, Decoder, OutputStream};

fn format_digits(digits: u64, size: f32, color: Color32) -> RichText {
    RichText::new(format!("{:02}", digits))
        .font(FontId::monospace(size))
        .color(color)
}

pub(crate) fn display_digits(strip: &mut Strip, dur: Duration, color: Color32, size: f32) {
    let hours = dur.as_secs() / 3600;
    let minutes = (dur.as_secs() / 60) % 60;
    let seconds = dur.as_secs() % 60;
    let hours = format_digits(hours, size, color);
    let minutes = format_digits(minutes, size, color);
    let seconds = format_digits(seconds, size, color);

    strip.strip(|strip| {
        strip
            .sizes(Size::relative(0.33), 3)
            .cell_layout(Layout::centered_and_justified(Direction::TopDown))
            .horizontal(|mut strip| {
                strip.cell(|ui| {
                    ui.label(hours);
                });
                strip.cell(|ui| {
                    ui.label(minutes);
                });
                strip.cell(|ui| {
                    ui.label(seconds);
                });
            });
    });
}

pub(crate) fn alarm(source: Vec<u8>) {
    let source = Cursor::new(source);
    let source = Decoder::new(source).unwrap();

    let (_stream, stream_handle) = OutputStream::try_default().unwrap();

    let dur = if let Some(dur) = source.total_duration() {
        dur
    } else {
        Duration::from_secs(10)
    };

    let _ = stream_handle.play_raw(source.convert_samples());
    std::thread::sleep(dur);
}
