use std::time::Duration;

pub const AIRHORN: &[u8] = include_bytes!("../resources/airhorn_alarm.mp3");
pub const PREDATOR_FONT: &[u8] = include_bytes!("../resources/Predator.ttf");

pub(crate) const MIN_REPAINT: Duration = Duration::from_millis(100);
pub(crate) const MAX_REPAINT: Duration = Duration::from_millis(250);

pub(crate) const DIGIT_FACTOR: f32 = 0.4;
pub(crate) const TEXT_FACTOR: f32 = 0.2;

pub mod cli;
pub mod timer;
mod util;
