use std::{sync::mpsc::Sender, time::Duration};

use eframe::egui::{Color32, Direction, Layout, RichText, Ui};
use egui_extras::{Size, StripBuilder};

use super::state::NextTimerState;
use crate::util::display_digits;

pub(crate) fn two_rows(
    ui: &mut Ui,
    buttons: &[(RichText, NextTimerState)],
    sender: Sender<NextTimerState>,
    remaining: Duration,
    digit_color: Color32,
    digit_size: f32,
) {
    StripBuilder::new(ui)
        .size(Size::relative(0.33333))
        .size(Size::remainder())
        .cell_layout(Layout::centered_and_justified(Direction::LeftToRight))
        .vertical(|mut strip| {
            strip.strip(|pstrip| {
                pstrip
                    .sizes(Size::remainder(), buttons.len())
                    .cell_layout(Layout::centered_and_justified(Direction::TopDown))
                    .horizontal(|mut pstrip| {
                        for (button, signal) in buttons.iter() {
                            pstrip.cell(|ui| {
                                if ui.button(button.clone()).clicked() {
                                    sender.send(*signal).unwrap();
                                }
                            });
                        }
                    });
            });

            display_digits(&mut strip, remaining, digit_color, digit_size);
        });
}

pub(crate) fn one_row(
    ui: &mut Ui,
    buttons: &[(RichText, NextTimerState)],
    sender: Sender<NextTimerState>,
) {
    StripBuilder::new(ui)
        .sizes(Size::remainder(), buttons.len())
        .cell_layout(Layout::centered_and_justified(Direction::TopDown))
        .horizontal(|mut strip| {
            for (button, signal) in buttons.iter() {
                strip.cell(|ui| {
                    if ui.button(button.clone()).clicked() {
                        sender.send(*signal).unwrap();
                    }
                });
            }
        });
}
