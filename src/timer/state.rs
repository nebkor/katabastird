use std::time::{Duration, Instant};

#[derive(Debug, Clone, Copy)]
pub(crate) enum TimerState {
    Unstarted,
    Paused(ChronoState),
    Running(ChronoState),
    Finished,
}

// this is a bit of a hack to deat with not being able to have const instances of the regular
// timerstate.
#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) enum NextTimerState {
    Unstarted,
    Paused,
    Running,
    // no need for finished, there will never be a button to click on that says "finished"
}

impl PartialEq for TimerState {
    fn eq(&self, other: &Self) -> bool {
        matches!(
            (self, other),
            (Self::Paused(_), Self::Paused(_))
                | (Self::Running(_), Self::Running(_))
                | (Self::Unstarted, Self::Unstarted)
                | (Self::Finished, Self::Finished)
        )
    }
}

impl Eq for TimerState {}
impl Eq for NextTimerState {}

#[derive(Debug, Clone, Copy)]
pub(crate) struct ChronoState {
    pub updated: Instant,
    pub remaining: Duration,
}
