use eframe::egui::Vec2;
use katabastird::timer::Timer;

fn main() {
    let options = eframe::NativeOptions {
        renderer: eframe::Renderer::Wgpu,
        max_window_size: Some(Vec2::new(1400.0, 800.0)),
        min_window_size: Some(Vec2::new(966.0, 600.0)),
        ..Default::default()
    };

    eframe::run_native(
        "katabastird",
        options,
        Box::new(move |cc| Box::new(Timer::new(cc))),
    );
}
